const express = require("express");
const db = require("../db");
const router = express.Router();


/**
 * @description test
 */
router.post("/getFirends", (req, res) => {
    db.query('SELECT * FROM friends where mb_id = ?', [req.query.mbId], (err, rows) => {

        if (err) {
            throw err;
        }

        res.send(rows);
    });
});

module.exports = router;
