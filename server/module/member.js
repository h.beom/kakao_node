const express = require("express");
const db = require("../db");
const router = express.Router();
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const randomstring = require('randomstring');
const randtoken = require('rand-token');
/**
 * @description 회원가입
 */
router.post("/signup", (req, res) => {
    let value = req.body;
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(value.pwd, salt, (err, hash) => {
            if (err) throw err;
            value.pwd = hash;
            db.query('INSERT INTO member(email, pw, name, nickname, birth_date, sex, phone, reg_date) VALUES (?, ?, ?, ?, ?, ?, ?, now())', [value.email, value.pwd, value.memberName, value.nickname, value.birthDate, value.sex, value.phoneNumber], (err, rows) => {
                if (err) {
                    throw err;
                }
                res.send({success: true})
            });
        });
    });
});

/**
 * @description 회원가입
 */
router.post("/getLogin", (req, res) => {
    let value = req.body;
    db.query('SELECT * FROM member WHERE email = ?', [value.email], (err, rows) => {
        if (err) {
            throw err;
        }
        if (rows.length === 0) {
            return res.json({success: false, err: '없는 계정입니다.'});
        } else {
            let user = rows[0];
            bcryptjs.compare(value.pwd, rows[0].pw)
                .then(isMatch => {
                    if (isMatch) {
                        let payload = {
                            mb_id: user.mb_id
                        };

                        var refreshToken = randtoken.uid(256);
                        db.query('UPDATE member SET refresh_token = ? WHERE mb_id = ?', [refreshToken, user.mb_id]);
                        jwt.sign(payload, 'fesTivAlL1veSEcretKey', {expiresIn: 3600 * 24 * 30}, (err, token) => {
                            res.header('Access-Control-Allow-Credentials', true);
                            res.cookie('refreshToken', refreshToken, {
                                expires: new Date('2100-01-01'),
                                httpOnly: true,
                                signed: true
                            });
                            return res.json({
                                success: true,
                                accessToken: 'Bearer ' + token,
                            });
                        });
                    } else {
                        return res.json({success: false, err: "비밀번호가 일치하지 않습니다."});
                    }
                })
        }
    });
});



/**
 * @description 정보 가져오기
 */
router.get("/me", (req, res) => {

    if (req.user) {
        return res.json({success: true, user: req.user});
    } else {
        if (req.headers.Authorization) {
            console.info(jwt.verify(req.headers.Authorization));
        }
        return res.json({success: false});
    }
});
module.exports = router;
