const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const db = require("../db");

const keys = 'fesTivAlL1veSEcretKey';

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys;

module.exports = passport => {
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {

            db.query('SELECT * FROM member where mb_id = ?', [jwt_payload.mb_id], (err, rows) => {
                if (err) {
                    throw err;
                }
                if (rows.length === 1)
                    return done(null, rows[0]);
                else
                    return done(null, false);
            });

    }));
};
